angular.module('ClinvarApp', ["ngramApp", "uswds"])
    .controller('gridController', ['$scope', 'debounce', function ($scope, debounce) {
        $scope.query = {matching: ''};
        $scope.expanded = false;
        $scope.toggle = function (expanded) {
            $scope.$broadcast(expanded ? 'gridtable.expandSubgrid' : 'gridtable.collapseSubgrid');
        };
        var _getSericeConfig = function (collection) {
            var _solr_conf /**solr config**/ = {
                serviceEngine: 'solr',
                serviceQueryUrl: 'http://<server>:<port>/solr/' + collection + '/select?wt=json&indent=true&json.wrf=?'
            }, _ngram_conf /**ngram config**/ = {
                serviceEngine: 'ngram',
                serviceQueryUrl: 'http://<server>:<port>/search/ngram?',
                NgramBackEndConfigurations: {
                    collection: collection,
                    schema: 'clinvar'
                }
            };
            return _ngram_conf;
        };
        $scope.getSubgridConfig = function (id) {
            return angular.merge({
                colModel: [
                    {"name": "id", "label": "id", "align": "center", "hidden": true},
                    {
                        "name": "VariationID",
                        "label": "VID",
                        "tips": "Variation ID",
                        "align": "center",
                        "hidden": false,
                        "width": 50
                    },
                    {
                        "name": "VariationName",
                        "label": "Variation Name",
                        "tips": "Variation Name",
                        "align": "center",
                        "hidden": true,
                        "width": 300
                    },
                    {"name": "AlleleOrigin", "label": "Allele Origin", "align": "center", "hidden": false},
                    {"name": "SCV", "label": "SCV", "align": "center", "hidden": true},
                    {"name": "InterpretedCondition", "label": "Interpreted Condition"},
                    {"name": "FamilyHistory", "label": "Family History", "align": "center", "hidden": true},
                    {"name": "EthnicityPopulationGroup", "label": "Ethnicity Population Group"},
                    {"name": "GeographicOrigin", "label": "Geographic Origin", "align": "center", "hidden": false}
                ],
                sortname: 'VariationID',
                rownumbers: true,
                autoLoad: true,
                minHeight: 10,
                rowNum: 50,
                pager: null
            }, _getSericeConfig('clinvarobs'));
        };

        $scope.gridConfig = angular.merge({
            //subGrid Configurations
            subGrid: true,
            subGridExpanded: $scope.expanded,
            subGridRowExpandedConfigCallback: function (subgrid_id, row_id, row_data) {
                return $scope.getSubgridConfig();
            },
            subGridRowExpandedQueryCallback: function (subgrid_id, row_id, row_data) {
                return {
                    matching: row_data.id + '_*'
                };
            },
            //colModel configurations
            colModel: [
                {"name": "id", "label": "id", "align": "center", "hidden": true},
                {"name": "VariationID", "label": "VID", "tips": "Variation ID", "align": "center", "width": 50},
                {
                    "name": "VariationName",
                    "label": "Variation Name",
                    "tips": "Variation Name",
                    "align": "center",
                    "width": 300
                },
                {"name": "SCV", "label": "SCV", "align": "center", "hidden": false, "width": 100},
                {"name": "Interpretation", "label": "Interpretation", "align": "center", "hidden": false, "width": 100},
                {"name": "SubmitterName", "label": "Submitter", "align": "center", "hidden": false},
                {
                    "name": "DateSubmission",
                    "label": "Date Submission",
                    "align": "center",
                    "width": 100
                },
                {"name": "CollectionMethod", "label": "Collection Method", "align": "center", "hidden": true},
                {"name": "ReviewStatus", "label": "Review Status", "align": "center", "hidden": true}
            ],
            rowList: [5,10],
            useLocalData: false,
            showHideColumns: true,
            adjustGridWidthOnWindowsResize: true,
            adjustGridWidthOnColumnChange: true,
            adjustGridWidthOnInit: false,
            shrinkToFit: true,
            minWdth: 400,
            rownumbers: true,
            autoLoad: true
        }, _getSericeConfig('clinvar'));

        $scope.startOver = function (event) {
            $scope.query.matching = '';
            $scope.$broadcast('gridtable.reload');
        };
        $scope.performSearchDebounced = debounce(function (event) {
            $scope.$broadcast('gridtable.reload');
        }, 600, false);

    }]);