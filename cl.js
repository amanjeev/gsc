#!/usr/bin/env node

var dload = require('./index.js');
var program = require('commander');
var fs = require('fs');
var path = require('path');

/*
A command line util to call for easy download
 */

program
    .arguments('<file>')
    .action(function (file) {
        // run both
        var asyncDir = path.join(__dirname, 'download_async');
        if (!fs.existsSync(asyncDir)) {
            fs.mkdirSync(asyncDir);
        }

        var syncDir = path.join(__dirname, 'download_sync');
        if (!fs.existsSync(syncDir)) {
            fs.mkdirSync(syncDir);
        }
        var asyncDownloadObj = dload.AsyncDload(file);
        var syncDownLoadObj = dload.SyncDload(file);
    })
    .parse(process.argv);