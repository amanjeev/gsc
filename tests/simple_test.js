var dload = require('../index.js');
var fs = require('fs');
var path = require('path');

describe("the downloads work", function () {
    describe("the async downloads work", function () {
        it("download directory has files", function () {
            var asyncDownloadObj = dload.AsyncDload(path.join(__dirname, '../data/urls.txt'));
            fs.readdir(path.join(__dirname, '../download_async/'), function (err, files) {
                expect(files.length).toBeGreaterThan(0);
            });
        });
    });


    describe("the blocking downloads work", function () {
        it("download directory has files", function () {
            var syncDownloadObj = dload.SyncDload(path.join(__dirname, '../data/urls.txt'));
            fs.readdir(path.join(__dirname, '../download_sync/'), function (err, files) {
                expect(files.length).toBeGreaterThan(0);
            });
        });
    });
});

