var path = require('path');
var fs = require('fs');
var http = require('http');
var async = require("async");


function AsyncDload(filename) {
    /**
     * Class that downloads and saves the files from twitter asynchronously.
     * This is achieved via async module.
     * @type {*}
     */
    var filename = filename || '../data/urls.txt';

    fs.readFile(filename, 'utf8', function (err, data) {
        dataArray = data.split('\n');
        dataArray = dataArray.map(function (val) {
            return val.split('   ')[1];
        });

        function saveFiles(url) {
            var fileDestination = path.join(__dirname, '../download_async/' + url.split('/').pop(-1).toLowerCase().replace("%20", "-"));
            console.log(fileDestination);

            var file = fs.createWriteStream(fileDestination);

            var request = http.get(url, function (response) {
                response.pipe(file);
                file.on('finish', function () {
                    // console.log('Saved file via async');
                    file.close();
                })
            }).on('error', function (err) {
                fs.unlink(fileDestination);
            });
        }

        // use async parallel to run tasks in parallel
        // this is non-blocking
        var dataArrayFuncs = dataArray.map(function (item) {
            return function (callback) {
                callback(null, saveFiles(item));
            }
        });

        async.parallel(dataArrayFuncs, function (err, results) {
        });

    })
}


function SyncDload(filename) {
    /**
     * Class that downloads and saves the files from twitter in a blocking way.
     * @type {*}
     */
    var filename = filename || '../data/urls.txt';

    fs.readFile(filename, 'utf8', function (err, data) {
        dataArray = data.split('\n');
        dataArray = dataArray.map(function (val) {
            return val.split('   ')[1];
        });

        function saveFiles(url) {
            var fileDestination = path.join(__dirname, '../download_sync/' + url.split('/').pop(-1).toLowerCase().replace("%20", "-"));

            var file = fs.createWriteStream(fileDestination);

            var request = http.get(url, function (response) {
                response.pipe(file);
                file.on('finish', function () {
                    // console.log('Saved file via blocking');
                    file.close();
                })
            }).on('error', function (err) {
                fs.unlink(fileDestination);
            });
        }

        // forEach is blocking
        // so this is blocking
        dataArray.forEach(function (url) {
            saveFiles(url);
        });

    });
}

// export both modules
module.exports = exports = {
    AsyncDload,
    SyncDload
};