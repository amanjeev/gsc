# Demo app for GSC

This is an exercise in creating a Nodejs commandline app that allows for download of a bunch of urls from twitter. The main idea is to showcase async vs blocking style in JS.
 
 
 ## Install
 
 * `git clone git clone git@bitbucket.org:amanjeev/gsc.git`
 * `cd gsc`
 * `npm install && npm link`
 
 
 ## Run 

 * `asyncload data/urls.txt`
 
 
 ## Test
 
 * `npm test`